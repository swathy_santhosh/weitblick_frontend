import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//Importing Components 
import { FullLayoutComponent } from './layouts/full-layout.component';
import { SimpleLayoutComponent } from './layouts/simple-layout.component';
import { UserComponent } from './user_profile/user.component';
import { LoginComponent } from './auth/sign-in/login.component';
import { LogOutComponent } from './auth/logout.component';
import { TransactionComponent } from './transactions/transaction.component';
import { TransactionHistoryComponent } from './transaction_history/transaction_history.component';
import { AccountSummaryComponent } from './account_summary/account_summary.component';

import { AuthGuard } from './auth/auth.guard';

export const routes: Routes = [

    // otherwise redirect to home
    {
        path: '',
        redirectTo: 'auth',
        pathMatch: 'full'
    },

    {
        path: 'banking/entry',
        component: FullLayoutComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'Home',
        },
        children: [
            {
                path: 'account_summary',
                component: AccountSummaryComponent
            },

            {
                path: 'user',
                component: UserComponent
            },
            {
                path: 'auth/logout',
                component: LogOutComponent
            },
            {
                path: '',
                children: [
                    { path: 'deposit', component: TransactionComponent },
                    { path: 'withdraw', component: TransactionComponent },
                    { path: 'transfer', component: TransactionComponent },
                ]
            },
            {
                path: 'transaction_statements',
                component: TransactionHistoryComponent
            },
            {
                path: 'my_profile',
                component: UserComponent
            }


        ]
    },
    {
        path: '',
        component: SimpleLayoutComponent,
        data: {
            title: 'Auth'
        },
        children: [
            {
                path: 'auth',
                component: LoginComponent
            }

        ]
    }


];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]

})
export class AppRoutingModule { }