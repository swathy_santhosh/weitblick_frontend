import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';
import { Transaction } from '../models/transaction';
import { Card } from '../models/card';


@Component({
  templateUrl: './account_summary.component.html'

})
export class AccountSummaryComponent implements OnInit {
  acc_summary: Card;
  constructor(private router: Router) { }
  /**
   * Function to view Statement
   * 
   */
  view_statement() {
    this.router.navigate(["/banking/entry/transaction_statements"]);
  }
  /**
   * Initializing function
   */
  ngOnInit() {
    this.acc_summary = JSON.parse(localStorage.getItem("currentUser"));
  }

}
