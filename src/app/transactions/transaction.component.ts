import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { Transaction } from '../models/transaction';
import { Card } from '../models/card';
import { TransactionService } from './transaction.service';
import { AuthenticationService } from '../auth/auth.service';
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  templateUrl: './transaction.component.html',
  providers: [TransactionService]

})
/**
 * TransactionComponent class to define methods for making transaction.
 */
export class TransactionComponent implements OnInit {
  transaction: Transaction;
  transaction_mode: string;
  currentUser: Card;
  isSuccess: boolean;
  showError:boolean=false;
  error:String;
  constructor(private router: Router, private route: ActivatedRoute, 
  private transactionService: TransactionService, private authService: AuthenticationService) { }

  /**
   * Function to make transaction such as deposit,withdraw (or) transfer.
   * @param obj 
   */
  transact(transactionForm : NgForm) {
    let transObj: any = {};
    transObj.amount = this.transaction.amount;
    transObj.is_transfer = this.transaction.is_transfer;
    transObj.transaction_mode = this.transaction_mode;
    if (this.transaction_mode == "transfer") {
      transObj.reason = this.transaction.reason;
      transObj.dest_id = this.transaction.dest_card_no;
    }
    this.transactionService.transact(transObj).subscribe(data => {
      this.isSuccess = true;
      this.authService.updateAccountBalance(data['transaction']['current_balance']);
      transactionForm.reset();    
      this.updateBalance(data['transaction']['current_balance']);
    },error=>{
      this.showError = true; 
      this.error  ="";   
      let errorObj = error.error["errors"];
       for(let obj in errorObj ){ 
           for(let item of errorObj[obj]){
                this.error  += obj + ": "+item+"\n";
           }
       }              
    });  
  }

  /**
   * Function to update user's current balance
   * @param amount 
   */

  updateBalance(amount){
     this.currentUser.account_balance = amount;
  }



  ngOnInit() {
    this.transaction = new Transaction();
    this.route.url.subscribe(params => {
      this.transaction_mode = params[0].path;
      if (this.transaction_mode == "transfer") {
        this.transaction.is_transfer = 1;
      } else {
        this.transaction.is_transfer = 0;
      }
    });

    this.currentUser = this.authService.getUser();
  }


}
