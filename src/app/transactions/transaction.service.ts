import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Transaction } from '../models/transaction';
import { environment } from "../../environments/environment";
import { Card } from '../models/card';


@Injectable({ providedIn: 'root' })
/**
 * TransactionService class to define http methods to call REST API endpoints.
 */
export class TransactionService {

    constructor(private httpClient: HttpClient) { }

    /**
     * Function to call making transaction
     * @param transObj 
     */
    public transact(transObj: Transaction): Observable<Transaction> {
        return this.httpClient.post<Transaction>(environment.api_url + transObj.transaction_mode, transObj);
    }
    /**
     * Function to retrieve transaction history of the currently logged in card.
     */
    public showTransaction(): Observable<Transaction[]> {
        return this.httpClient.get<Transaction[]>(environment.api_url + "getTransactionHistory");
    }

}