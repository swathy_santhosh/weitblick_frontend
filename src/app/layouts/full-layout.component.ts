import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


import { Card } from '../models/card';


@Component({
  selector: 'app-layouts',
  templateUrl: './full-layout.component.html'

})
/**
 * FullLayoutComponent class
 */
export class FullLayoutComponent implements OnInit {

  loggedin_card: Card;  
  constructor() { }

  ngOnInit(): void {
    this.loggedin_card = new Card();
    
  }

}
