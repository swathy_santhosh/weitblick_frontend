import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../auth/auth.service';
import { environment } from '../../environments/environment';
import { Card } from '../models/card';

@Component({
  templateUrl: './user.component.html'

})
/**
 * UserComponent class to get user related details.
 */
export class UserComponent implements OnInit {

  user: Card;
  constructor(private authService: AuthenticationService, private router: Router) { }



  ngOnInit() {
    this.user = this.authService.getUser();
  }

}
