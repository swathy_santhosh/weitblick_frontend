import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TransactionService } from '../transactions/transaction.service';
import { environment } from '../../environments/environment';
import { Transaction } from '../models/transaction';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Component({
  templateUrl: './transaction_history.component.html',
  providers: [TransactionService]
})
/**
 * TransactionHistoryComponent class to define methods for rendering transaction history .
 */
export class TransactionHistoryComponent implements OnInit {
  trans_logs: Transaction[];
  totalRecords: number;
  closing_balance: string;
  loading: boolean;
  error: string;
  showError: boolean;
  constructor(private tranService: TransactionService, private router: Router) { }

  paginate(event) {
  }


  ngOnInit() {
    this.loading = true;
    let user = JSON.parse(localStorage.getItem("currentUser"));
    this.closing_balance = user.account_balance;
    this.tranService.showTransaction().subscribe(res => {
      this.trans_logs = res['transactions'];
      this.totalRecords = this.trans_logs.length;
      this.loading = false;
    }, error => {
      this.error = error.error.status;
      this.showError = true;
    });

  }

}
