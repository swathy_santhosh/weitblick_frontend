import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Card } from '../../models/card';
import { AuthenticationService } from '../auth.service';
import { NgForm } from '@angular/forms';

@Component({
  templateUrl: './login.component.html',

})

/**
 * Class to implement login component functions
 */
export class LoginComponent {
  title = 'Weitblick ATM';
  card: any={};
  error: any;
  showError: boolean;
  constructor(private router: Router, private authService: AuthenticationService) { }

  /**
   * Function to authenticate user
   * @param loginRequestForm 
   */ 
  authenticate(loginRequestForm: NgForm) {     
    this.showError = false;  
    this.authService.login(this.card).subscribe(currentUser => {
      this.router.navigate(['/banking/entry/account_summary']);
    },
      error => {
        loginRequestForm.controls['password'].reset();       
        this.error = error.error.error;        
        this.showError = true;
      });
  }
  

}