import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { catchError } from 'rxjs/operators';
import { environment } from "../../environments/environment";
import {throwError, of } from 'rxjs';
import { Router } from '@angular/router';

import { Card } from '../models/card';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<Card>;
    public currentUser: Observable<Card>;     
    constructor(private http: HttpClient,private router:Router) {        
        this.currentUserSubject = new BehaviorSubject<Card>(this.getUser());
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): Card {  
        return this.currentUserSubject.value;
    }

    login(requestPayload:any):Observable<Card>{
       return this.http.post<any>(environment.api_url+"login", requestPayload)
            .pipe(map(data => { 
                let body = data['card'];
                // login successful if there's a jwt token in the response
                if (data['token']) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify((data['card'])));   
                    localStorage.setItem('token', JSON.stringify(data['token']));                                                          
                    this.currentUserSubject.next(data['card']);                                          
                }
                 
                return body;
            }),catchError(error=>{               
                return throwError(error);
            }));  
            
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('token');
        localStorage.removeItem('currentUser');
        localStorage.removeItem('user_account_balance');        
        this.currentUserSubject.next(null);
        this.router.navigate(['']);
    }

    public get loggedIn(): boolean{
        return localStorage.getItem('token') !==  null;
    }

    getUser(){        
        if(localStorage.getItem('currentUser')){          
            let user:Card = JSON.parse(localStorage.getItem('currentUser'));           
            return user;             
        }         
        return null;
    }
    
    updateAccountBalance(account_balance){        
        let currentLogUser:Card = this.getUser();
        if(currentLogUser){
            localStorage.setItem('user_account_balance',account_balance);
            currentLogUser.account_balance = account_balance;
            this.setCurrentUser(currentLogUser);              
        }
    }

    setCurrentUser(user){
        localStorage.setItem('currentUser',JSON.stringify(user));
    }

    getAccountBalance(){
        let user = this.getUser();
        if(user){
            return JSON.parse(user.account_balance);
        }
    }


    
}