/**
 * Class to define metadata of Card model
 */
export class Card {
    id: number;
    card_no: number;
    user_card_no: string;
    uuid: string;
    password: number;
    token?: string;
    user_name: string;
    user_email: string;
    user_phone: string;
    account_balance: string;

}