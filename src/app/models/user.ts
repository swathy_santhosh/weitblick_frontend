/**
 * Class to define metadata of User model
 */
export class User {
    id: number;
    first_name: string;
    last_name: string;
    email: string;
    phone_no: string;

}