/**
 * Class to define metadata of Transaction model
 */
export class Transaction {
    id: number;
    amount: number;
    reason: string;
    dest_card_no: number;
    is_transfer: number;
    src_card_no: number;
    date_of_transaction: string;
    transaction_mode: string;
}