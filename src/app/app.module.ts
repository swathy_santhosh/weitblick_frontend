import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppComponent } from './app.component';

// Routing Module
import { AppRoutingModule } from './app-routing.module';
import { TableModule } from 'primeng/table';

//Number directive
import { NumberDirective } from './shared/only-number.directive';

// Layouts
import { FullLayoutComponent } from './layouts/full-layout.component';
import { SimpleLayoutComponent } from './layouts/simple-layout.component';

// Components
import { TransactionHistoryComponent } from './transaction_history/transaction_history.component';
import { UserComponent } from './user_profile/user.component';
import { LoginComponent } from './auth/sign-in/login.component';
import { LogOutComponent } from './auth/logout.component';
import { TransactionComponent } from './transactions/transaction.component';
import { AccountSummaryComponent } from './account_summary/account_summary.component';

//JWT interceptor
import { JwtInterceptor } from './auth/jwt.interceptor';
//Error Interceptor
import { ErrorInterceptor } from './error_handlers/error.interceptor';


@NgModule({
  declarations: [
    AppComponent,
    FullLayoutComponent,
    SimpleLayoutComponent,
    NumberDirective,
    UserComponent,
    LoginComponent,
    LogOutComponent,
    TransactionComponent,
    TransactionHistoryComponent,
    AccountSummaryComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    TableModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
  //  { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
