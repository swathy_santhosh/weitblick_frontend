# Weitblick Full Stack Developer Test- Frontend App

The final POC for the Weitblick Developer Test. This Repository contains the frontend that powers the application. The Laravel backend instructions will be provided in the Backend Repository.

## Prequisites

The below software are required to run the Angular application

- Node 
- Angular CLI

## Architecture

This solution is built based on the requirement to build ATM web app and is developed using Angular 7 with RxJs and Typescript. This application uses token based authentication to authenticate user to the backend application.
This application is a responsive single page application which communicates the API using RESTAPI layer.The application is designed to mock ATM machine and hence the screen size is designed to simulate the user experience of an actual ATM screen.

When the User logins, the system would call the login endpoint first to get a JWT Token which should then be passed as an authorization header with a Bearer string so that the authenticated endpoints are accessible. The token is set to 60 minutes (ttl). In production system, this will be set to 15 minutes for security. After 1 hour, the token will be expired.

If the token is expired or if the user experience 401 error when accessing the application, the user will be logged out automatically. This is achieved by implementing HttpInterceptor to capture 401 error.The token is attached to the each request on Authorization Header with string "Bearer" followed by token. 

All the routes are protected except login route. The password is secure and is hashed using PHP Argon hashing algorithm in the backend and the sensitive details such as card number are secured in the api response. 

The client side validation is taken care of and the error handling is handled and the user will be notified by the message receieved from API. The application uses PrimeNg for rendering data on the primeng components and 
bootstrap4 for designing stylesheets. 

## List of Technology stack Used
- Node                   
- Angular CLI        
- Angular 7
- PrimeNg
- Bootstrap 4

## Setup Instruction
 
1. Install node (This application need node 8.x or 10.x)

2. Install angular/cli 
    npm install -g @angular/cli
3. Navigate to the project directory.
        npm install
4. The above step 3 installs the required dependencies to run this application.

## IP Address of the Docker Engine (!!!!Important for Connecting the Client)
a. Make note of docker machine by running this below command in the docker terminal window:
$ docker-machine ip
b. Update the following file with the IP retrieved from docker
    File to be updated: {APP_ROOT_DIR}/environments/environment.ts
    Property to be updated: "api_url".
     After updating, your api_url value should look like this: "http://192.168.99.100/api/",
     the ip address may differs based on your container

5. Once the above steps completes, run the following command to start application.
    $ ng serve
    This should run the application on http://localhost:4200 or whichever port it says on the terminal.
    Open the link in browser, you should be able to view the login page that is redirected to http://localhost:4200/auth

This is all that's required to start and run the application.

## Application Start up Workflow

1. Once the application is up, enter the 16 digit card number and pin.
2. The application takes you to the account summary page where you can see the summary of your account balance and the card details.
3. The application is designed to navigate menu and to render data.
4. Please remember that the newly created token will be expired in 1 hour and you will be logged out of the application.